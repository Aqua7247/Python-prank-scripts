# this script opens a bunch of windows and upon trying to close it, plays a sound and opens a bunch of windows

import tkinter as tk  
from random import randrange
import subprocess
from threading import Thread


x = 100
y = 100

root = tk.Tk()
root.title ('Updating')
root.resizable(False,False)
root.geometry(f'600x400+{x}+{y}')

# change this to the windows equivallent, the rest of the script should work on windows
def start_sound():
    subprocess.call(["afplay", "noise1.wav"])

def on_activation():
    Thread(target=start_sound).start()
    for i in range (1,20):
        
        root = tk.Tk()
        root.title ('HA HA')
        root.resizable(False,False)
        root.wm_attributes("-topmost", 1)
        ##root.focus_force()
        x = randrange(801)
        y = randrange(801)
        root.geometry(f'600x400+{x}+{y}')
        root.protocol("WM_DELETE_WINDOW", on_activation)
        

on_activation()

root.protocol("WM_DELETE_WINDOW", on_activation)
start_sound
root.mainloop()

